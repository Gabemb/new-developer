# Barrel Frontend Developer Test

App scaffolding generated with [Create React App](https://github.com/facebookincubator/create-react-app).

Social media svg icons courtesy of [IconMonstr](https://iconmonstr.com).

Run `npm start` or `yarn start` in the project folder for the development build. Use `npm run build` or `yarn build` for production bundle.
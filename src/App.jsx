import React, { Component } from 'react';
//Components
import Header from './components/Header/Header.jsx'
import Intro from './components/Intro/Intro.jsx'
import LatestArticle from './components/LatestArticle/LatestArticle.jsx'
import RecentArticles from './components/RecentArticles/RecentArticles.jsx'
import Footer from './components/Footer/Footer.jsx'
//Styling
import './App.css';

export default class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header />
        <Intro />
        <section className="main-container">
          <LatestArticle />
        </section>
        <RecentArticles />
        <Footer />
      </div>
    );
  }
}

import React, { Component } from 'react';
import './LatestArticle.css';

export default class LatestArticle extends Component {
  constructor(props){
    super(props)

    this.state ={
      article: '',
    }
  }

  componentDidMount() {
    try {
      fetch('seed.json')
        .then((res) => res.json())
        .then((data) => {
          this.setState({article: data.articles[0]});
        })
        .catch((err) => console.error(`Problem encountered while fetching: ${err.message}`));     
    }
    catch (err) {
      console.error(`Problem encountered with fetch attempt: ${err.message}`)
    }
  }

  render() {
    const article = this.state.article;
    if (article) {
      return (
        <main role='main' className='latest'>
          <img className='main-img' src={`./images/${article.img}`} alt={article.img}></img>
          <article className='main' >
            <img className='icon' src={require(`../../assets/images/icons/${article.type}.svg`)} alt={`${article.type} icon`} />
            <h3 className='date'>{article.date}</h3>
            <h1 className='title main-title'>{article.title}</h1>
            <p className='content'>{article.content}</p>
            <a>
              {
                // If the article is a video
                article.type === 'Video'
                // render this text:
                ? 'Watch Video'
                // Else if the article is a gallery
                : article.type === 'Gallery'
                  // render this text:
                  ? 'View Gallery'
                  // Else render this text:
                  : 'Read More'
              }
            </a>
          </article>
        </main>
      );
    } else {
      return null;
    }
  }
}

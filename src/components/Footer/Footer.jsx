import React from 'react';
import logo from '../../assets/images/logos/www.knobcreek.com-1311011787501770.svg';
import fb from '../../assets/images/icons/social-media/facebook.svg';
import twt from '../../assets/images/icons/social-media/twitter.svg';
import './Footer.css';

const Footer = () => (
  <footer className='footer'>
    <section className="share-section">
      <p className='share'>SHARE ON</p>
      <img src={fb} className='social-media fb foot' alt='facebook-icon'></img>
      <img src={twt} className='social-media twt foot' alt='twitter-icon'></img>
    </section>
    <img src={logo} className='logo flg' alt='logo'></img>
  </footer>
);

export default Footer;

import React from 'react';
import './Article.css';

const Article = ({article:{date, img, title, type}, idx}) => (
  <article className='Article' >
    <img className='cover-image' src={`./images/${img}`} alt={img}/>
    <img className='icon' src={require(`../../assets/images/icons/${type}.svg`)} alt={`${type} icon`} />
    <h3 className='date'>{date}</h3>
    <h1 className='title'>{title}</h1>
    <a>
      {
        // If the article is a video
        type === 'Video'
        // render this text:
        ? 'Watch Video'
        // Else if the article is a gallery
        : type === 'Gallery'
          // render this text:
          ? 'View Gallery'
          // Else render this text:
          : 'Read More'
      }
    </a>
  </article>
);

export default Article;

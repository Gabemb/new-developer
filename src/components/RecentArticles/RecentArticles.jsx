import React, { Component } from 'react';
import Article from '../Article/Article.jsx';
import './RecentArticles.css';

export default class RecentArticles extends Component {
  constructor(props){
    super(props)

    this.state ={
      articles: [],
    }
  }

  componentDidMount() {
    try {
      fetch('seed.json')
        .then((res) => res.json())
        .then((data) => {
          this.setState({articles: data.articles});
        })
        .catch((err) => console.error(`Problem encountered while fetching: ${err.message}`));     
    }
    catch (err) {
      console.error(`Problem encountered in fetch attempt: ${err.message}`)
    }
  }

  render() {
    const articles = this.state.articles;
    return (
      <section className='Articles'>
        <h1 className='recent-header'>Recent Articles</h1>
        {
          // If my state hasn't been populated with an array of articles yet, or there simply aren't any articles yet,
          // then don't render anything.
          // otherwise render individual Article components with the right info.
          !articles
          ? null
          : articles.map((article, idx) => (
            //Skip the first article in our array
            !idx
            ? null
            : <Article article={article} idx={idx} key={`${article.date}${idx}`} />
          ))
        }
      </section>
    );
  }
}

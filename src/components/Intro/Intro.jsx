import React, { Component } from 'react';
import './Intro.css';

export default class Intro extends Component {
  constructor(props){
    super(props)

    this.state ={
      blurb: '',
    }
  }

  componentDidMount() {
    try {
      fetch('seed.json')
        .then((res) => res.json())
        .then((data) => {
          this.setState({blurb: data.intro});
        })
        .catch((err) => console.error(`Problem encountered while fetching: ${err.message}`));     
    }
    catch (err) {
      console.error(`Problem encountered with fetch attempt: ${err.message}`)
    }
  }

  render() {
    const blurb = this.state.blurb;
    return (
      <section className='intro'>
          <article className='headline'>
            <h1 className='intro-title'>{blurb.title}</h1>
            <p className='intro-blurb'>{blurb.body}</p>
          </article>
          <img className='bourbon' src='./images/www.knobcreek.com-1310894113736742.png' alt='bourbon'></img>
      </section>
    );
  }
}

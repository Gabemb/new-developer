import React from 'react';
import logo from '../../assets/images/logos/www.knobcreek.com-1311011787501770.svg';
import fb from '../../assets/images/icons/social-media/facebook.svg';
import twt from '../../assets/images/icons/social-media/twitter.svg';
import './Header.css';

const Header = () => (
  <header className='header'>
    <section className='sponsor'>Sponsored By</section>
    <img src={logo} className='logo hlg' alt='logo'></img>
    <p className='placeholder-logo'>L O G O</p>
    <section className='social-section'>
      <img src={fb} className='social-media fb head' alt='facebook-icon'></img>
      <img src={twt} className='social-media twt head' alt='twitter-icon'></img>
    </section>
  </header>
);

export default Header;
